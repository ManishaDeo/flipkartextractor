
module.exports = {
  implements: 'navigation/auth/action',
  parameterValues: {
    domain: 'easyordershop.com',
    country: 'US',
    store: 'easyordershop',
    zipcode: '',
  },
};
