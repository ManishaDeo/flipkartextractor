
module.exports = {
  implements: 'navigation/auth/doLogin',
  parameterValues: {
    domain: 'easyordershop.com',
    usernameSelector: '#cnt_core input[name="Email"]',
    passwordSelector: '#cnt_core input[name="password"]',
    buttonSelector: '#cnt_close > div > a.button_flex',
    loggedInSelector: '#showemail',
    country: 'US',
    store: 'easyordershop',
    zipcode: '',
  },
};
