
module.exports = {
  implements: 'navigation/auth/gotoLogin',
  parameterValues: {
    domain: 'easyordershop.com',
    loginPage: 'https://tork-usa.easyordershop.com/',
    country: 'US',
    store: 'easyordershop',
    zipcode: '',
  },
};
