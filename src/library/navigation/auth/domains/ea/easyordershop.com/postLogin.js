
module.exports = {
  implements: 'navigation/auth/postLogin',
  parameterValues: {
    domain: 'easyordershop.com',
    country: 'US',
    store: 'easyordershop',
    zipcode: '',
  },
};
