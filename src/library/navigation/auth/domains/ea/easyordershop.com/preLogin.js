
module.exports = {
  implements: 'navigation/auth/preLogin',
  parameterValues: {
    domain: 'easyordershop.com',
    country: 'US',
    store: 'easyordershop',
    zipcode: '',
  },
};
