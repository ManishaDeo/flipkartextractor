module.exports = {
  extends: 'navigation/goto/domains/am/amazon',
  parameterValues: {
    country: 'CA',
    domain: 'amazon.ca',
    store: 'amazon',
  },
};
