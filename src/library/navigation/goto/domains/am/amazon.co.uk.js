module.exports = {
  extends: 'navigation/goto/domains/am/amazon',
  parameterValues: {
    country: 'UK',
    domain: 'amazon.co.uk',
    store: 'amazon',
  },
};
