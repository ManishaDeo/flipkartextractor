module.exports = {
  extends: 'navigation/goto/domains/am/amazon',
  parameterValues: {
    country: 'US',
    domain: 'amazon.com',
    store: 'amazon',
  },
};
