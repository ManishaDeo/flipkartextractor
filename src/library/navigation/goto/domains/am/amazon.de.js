module.exports = {
  extends: 'navigation/goto/domains/am/amazon',
  parameterValues: {
    country: 'DE',
    domain: 'amazon.de',
    store: 'amazon',
  },
};
