module.exports = {
  extends: 'navigation/goto/domains/am/amazon',
  parameterValues: {
    country: 'ES',
    domain: 'amazon.es',
    store: 'amazon',
  },
};
