module.exports = {
  extends: 'navigation/goto/domains/am/amazon',
  parameterValues: {
    country: 'FR',
    domain: 'amazon.fr',
    store: 'amazon',
  },
};
