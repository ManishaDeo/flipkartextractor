module.exports = {
  extends: 'navigation/goto/domains/am/amazon',
  parameterValues: {
    country: 'IT',
    domain: 'amazon.it',
    store: 'amazon',
  },
};
