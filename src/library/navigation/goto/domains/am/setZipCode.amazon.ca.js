
module.exports = {
  implements: 'navigation/goto/setZipCode',
  parameterValues: {
    country: 'CA',
    domain: 'amazon.ca',
    store: 'amazon',
    zipcode: '',
  },
};
