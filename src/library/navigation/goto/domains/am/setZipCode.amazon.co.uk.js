
module.exports = {
  implements: 'navigation/goto/setZipCode',
  parameterValues: {
    country: 'UK',
    domain: 'amazon.co.uk',
    store: 'amazon',
    zipcode: '',
  },
};
