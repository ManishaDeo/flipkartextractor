
module.exports = {
  implements: 'navigation/goto/setZipCode',
  parameterValues: {
    country: 'DE',
    domain: 'amazon.de',
    store: 'amazon',
    zipcode: '',
  },
};
