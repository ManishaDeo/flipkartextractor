
module.exports = {
  implements: 'navigation/goto/setZipCode',
  parameterValues: {
    country: 'ES',
    domain: 'amazon.es',
    store: 'amazon',
    zipcode: '',
  },
};
