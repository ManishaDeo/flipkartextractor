
module.exports = {
  implements: 'navigation/goto/setZipCode',
  parameterValues: {
    country: 'FR',
    domain: 'amazon.fr',
    store: 'amazon',
    zipcode: '',
  },
};
