
module.exports = {
  implements: 'navigation/goto/setZipCode',
  parameterValues: {
    country: 'IT',
    domain: 'amazon.it',
    store: 'amazon',
    zipcode: '',
  },
};
