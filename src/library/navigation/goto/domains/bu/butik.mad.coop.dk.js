
module.exports = {
  implements: 'navigation/goto',
  parameterValues: {
    domain: 'butik.mad.coop.dk',
    timeout: null,
    jsonToTable: null,
    country: 'UK',
    store: 'madcoop',
    zipcode: '',
  },
};
