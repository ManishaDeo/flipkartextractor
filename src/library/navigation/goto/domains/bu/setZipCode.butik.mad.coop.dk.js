
module.exports = {
  implements: 'navigation/goto/setZipCode',
  parameterValues: {
    country: 'UK',
    domain: 'butik.mad.coop.dk',
    store: 'madcoop',
    zipcode: '',
  },
};
