
module.exports = {
  implements: 'navigation/goto',
  parameterValues: {
    domain: 'dyson.co.nz',
    timeout: null,
    jsonToTable: null,
    country: 'NZ',
    store: 'dyson',
    zipcode: '',
  },
};
