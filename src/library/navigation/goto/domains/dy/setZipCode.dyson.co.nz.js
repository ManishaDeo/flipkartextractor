
module.exports = {
  implements: 'navigation/goto/setZipCode',
  parameterValues: {
    country: 'NZ',
    domain: 'dyson.co.nz',
    store: 'dyson',
    zipcode: '',
  },
};
