
module.exports = {
  implements: 'navigation/goto',
  parameterValues: {
    domain: 'easyordershop.com',
    timeout: null,
    jsonToTable: null,
    country: 'US',
    store: 'easyordershop',
    zipcode: '',
  },
};
