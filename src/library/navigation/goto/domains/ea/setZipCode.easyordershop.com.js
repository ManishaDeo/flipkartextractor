
module.exports = {
  implements: 'navigation/goto/setZipCode',
  parameterValues: {
    country: 'US',
    domain: 'easyordershop.com',
    store: 'easyordershop',
    zipcode: '',
  },
};
