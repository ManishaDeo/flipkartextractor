
module.exports = {
  implements: 'navigation/goto',
  parameterValues: {
    domain: 'flipkart.com',
    timeout: 30000,
    jsonToTable: null,
    country: 'IN',
    store: 'flipkart',
    zipcode: '',
  },
};
