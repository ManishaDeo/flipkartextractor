
module.exports = {
  implements: 'navigation/goto/setZipCode',
  parameterValues: {
    country: 'IN',
    domain: 'flipkart.com',
    store: 'flipkart',
    zipcode: '',
  },
};
