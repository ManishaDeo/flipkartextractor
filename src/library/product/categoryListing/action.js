/**
 *
 * @param { { URL: string, storeId: string, zipcode: string,  } } inputs
 * @param { { store: any, country: any, zipcode: any, storeID: any, storeId: any  } } parameters
 * @param { ImportIO.IContext } context
 * @param { { execute: ImportIO.Action, paginate: ImportIO.Action, extract: ImportIO.Action } } dependencies
 */

async function implementation (inputs, parameters, context, dependencies) {
  const { URL } = inputs;
  const { execute, extract } = dependencies;
  const categoryURL = URL;
  const zipcode = inputs.zipcode || parameters.zipcode;
  const storeId = inputs.storeId || parameters.storeID || parameters.storeID;

  const newInput = { ...inputs, storeId, zipcode, categoryURL };

  const resultsReturned = await execute(newInput);
  if (!resultsReturned) {
    console.log('No results were returned');
    return;
  }

  await extract(newInput);
}
module.exports = {
  parameters: [
    {
      name: 'country',
      description: '2 letter ISO code for the country',
    },
    {
      name: 'store',
      description: 'store name',
    },
    {
      name: 'domain',
      description: 'The top private domain of the website (e.g. amazon.com)',
    },
    {
      name: 'zipcode',
      description: 'to set location',
      optional: true,
    },
    {
      name: 'storeID',
      description: 'Id of the store',
      type: 'string',
      optional: true,
    },
  ],
  inputs: [
    {
      name: 'URL',
      description: 'product listing url',
      type: 'string',
      optional: true,
    },
  ],
  dependencies: {
    execute: 'action:product/categoryListing/execute',
    extract: 'action:product/categoryListing/extract',
  },
  path: './categoryListing/stores/${store[0:1]}/${store}/${country}/categoryListing',
  implementation,
};
