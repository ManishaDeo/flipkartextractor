
module.exports = {
  implements: 'product/categoryListing',
  parameterValues: {
    country: 'US',
    store: 'easyordershop',
    domain: 'easyordershop.com',
    zipcode: '',
    storeID: null,
  },
};
