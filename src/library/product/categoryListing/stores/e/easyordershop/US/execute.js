
module.exports = {
  implements: 'product/categoryListing/execute',
  parameterValues: {
    country: 'US',
    store: 'easyordershop',
    domain: 'easyordershop.com',
    url: 'https://tork-usa.easyordershop.com/easyorder/infopr1a?mode=CNT',
    loadedSelector: '#cnt_core',
    noResultsXPath: null,
    zipcode: '',
  },
};
