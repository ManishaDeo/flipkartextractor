
module.exports = {
  implements: 'product/categoryListing/extract',
  parameterValues: {
    country: 'US',
    store: 'easyordershop',
    transform: null,
    domain: 'easyordershop.com',
    zipcode: '',
  },
};
