
module.exports = {
  implements: 'product/details',
  parameterValues: {
    country: 'CA',
    store: 'amazonMsPromos',
    domain: 'amazon.ca',
    zipcode: '',
  },
};
