
module.exports = {
  implements: 'product/details/createUrl',
  parameterValues: {
    domain: 'amazon.es',
    prefix: null,
    url: 'https://www.amazon.es/',
    country: 'ES',
    store: 'amazonMsPromos',
    zipcode: '',
  },
};
