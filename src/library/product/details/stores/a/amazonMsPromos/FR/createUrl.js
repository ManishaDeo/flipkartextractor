
module.exports = {
  implements: 'product/details/createUrl',
  parameterValues: {
    domain: 'amazon.fr',
    prefix: null,
    url: 'https://www.amazon.fr/',
    country: 'FR',
    store: 'amazonMsPromos',
    zipcode: '',
  },
};
