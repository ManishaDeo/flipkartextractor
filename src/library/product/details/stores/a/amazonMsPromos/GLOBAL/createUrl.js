
module.exports = {
  implements: 'product/details/createUrl',
  parameterValues: {
    domain: 'amazon.com',
    prefix: null,
    url: 'https://www.amazon.com/gp/goldbox?ref_=nav_cs_gb',
    country: 'GLOBAL',
    store: 'amazonMsPromos',
  },
};
