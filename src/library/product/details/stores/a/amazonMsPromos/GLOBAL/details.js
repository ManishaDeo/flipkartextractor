
module.exports = {
  implements: 'product/details',
  parameterValues: {
    country: 'GLOBAL',
    store: 'amazonMsPromos',
    domain: 'amazon.com',
    zipcode: '',
  },
};
