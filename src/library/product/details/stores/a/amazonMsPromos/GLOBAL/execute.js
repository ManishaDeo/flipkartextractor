
module.exports = {
  implements: 'product/details/execute',
  parameterValues: {
    country: 'GLOBAL',
    store: 'amazonMsPromos',
    domain: 'amazon.com',
    loadedSelector: null,
    noResultsXPath: null,
    zipcode: '',
  },
};
