
module.exports = {
  implements: 'product/details/createUrl',
  parameterValues: {
    domain: 'amazon.it',
    prefix: null,
    url: 'https://www.amazon.it/',
    country: 'IT',
    store: 'amazonMsPromos',
    zipcode: '',
  },
};
