
module.exports = {
  implements: 'product/details',
  parameterValues: {
    country: 'IT',
    store: 'amazonMsPromos',
    domain: 'amazon.it',
    zipcode: '',
  },
};
