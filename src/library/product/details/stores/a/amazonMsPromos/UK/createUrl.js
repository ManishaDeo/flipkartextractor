
module.exports = {
  implements: 'product/details/createUrl',
  parameterValues: {
    domain: 'amazon.co.uk',
    url: 'https://www.amazon.co.uk/',
    country: 'UK',
    store: 'amazonMsPromos',
  },
};
