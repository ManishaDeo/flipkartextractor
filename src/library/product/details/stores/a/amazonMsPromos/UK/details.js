
module.exports = {
  implements: 'product/details',
  parameterValues: {
    country: 'UK',
    store: 'amazonMsPromos',
    domain: 'amazon.co.uk',
    zipcode: '',
  },
};
