
module.exports = {
  implements: 'product/details/createUrl',
  parameterValues: {
    domain: 'amazon.com',
    prefix: null,
    url: 'https://www.amazon.com/',
    country: 'US',
    store: 'amazonMsPromos',
  },
};
