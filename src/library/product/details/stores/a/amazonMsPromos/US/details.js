
module.exports = {
  implements: 'product/details',
  parameterValues: {
    country: 'US',
    store: 'amazonMsPromos',
    domain: 'amazon.com',
    zipcode: '',
  },
};
