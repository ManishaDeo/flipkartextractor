
module.exports = {
  implements: 'product/details/createUrl',
  parameterValues: {
    domain: 'amazon.ca',
    url: 'https://www.amazon.ca',
    country: 'CA',
    store: 'amazonMsPromosDealId',
  },
};
