
module.exports = {
  implements: 'product/details',
  parameterValues: {
    country: 'CA',
    store: 'amazonMsPromosDealId',
    domain: 'amazon.ca',
    zipcode: '',
  },
};
