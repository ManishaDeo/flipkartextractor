
module.exports = {
  implements: 'product/details/createUrl',
  parameterValues: {
    domain: 'amazon.de',
    prefix: null,
    url: 'https://www.amazon.de/gp/goldbox',
    country: 'DE',
    store: 'amazonMsPromosDealId',
    zipcode: '',
  },
};
