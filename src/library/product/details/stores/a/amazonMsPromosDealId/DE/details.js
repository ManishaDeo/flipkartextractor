
module.exports = {
  implements: 'product/details',
  parameterValues: {
    country: 'DE',
    store: 'amazonMsPromosDealId',
    domain: 'amazon.de',
    zipcode: '',
  },
};
