
module.exports = {
  implements: 'product/details/createUrl',
  parameterValues: {
    domain: 'amazon.es',
    prefix: null,
    url: 'https://www.amazon.es/gp/goldbox',
    country: 'ES',
    store: 'amazonMsPromosDealId',
    zipcode: '',
  },
};
