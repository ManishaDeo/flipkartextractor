
module.exports = {
  implements: 'product/details',
  parameterValues: {
    country: 'ES',
    store: 'amazonMsPromosDealId',
    domain: 'amazon.es',
    zipcode: '',
  },
};
