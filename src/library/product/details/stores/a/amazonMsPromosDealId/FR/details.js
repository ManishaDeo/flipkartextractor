
module.exports = {
  implements: 'product/details',
  parameterValues: {
    country: 'FR',
    store: 'amazonMsPromosDealId',
    domain: 'amazon.fr',
    zipcode: '',
  },
};
