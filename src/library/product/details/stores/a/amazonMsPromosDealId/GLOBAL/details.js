
module.exports = {
  implements: 'product/details',
  parameterValues: {
    country: 'GLOBAL',
    store: 'amazonMsPromosDealId',
    domain: 'amazon.com',
    zipcode: '',
  },
};
