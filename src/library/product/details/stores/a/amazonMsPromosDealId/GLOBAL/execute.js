
module.exports = {
  implements: 'product/details/execute',
  parameterValues: {
    country: 'GLOBAL',
    store: 'amazonMsPromosDealId',
    domain: 'amazon.com',
    loadedSelector: null,
    noResultsXPath: null,
    zipcode: '',
  },
};
