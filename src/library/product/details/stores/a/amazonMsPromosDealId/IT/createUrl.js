
module.exports = {
  implements: 'product/details/createUrl',
  parameterValues: {
    domain: 'amazon.it',
    prefix: null,
    url: 'https://www.amazon.it/gp/goldbox',
    country: 'IT',
    store: 'amazonMsPromosDealId',
    zipcode: '',
  },
};
