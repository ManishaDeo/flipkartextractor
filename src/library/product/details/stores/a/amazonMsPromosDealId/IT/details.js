
module.exports = {
  implements: 'product/details',
  parameterValues: {
    country: 'IT',
    store: 'amazonMsPromosDealId',
    domain: 'amazon.it',
    zipcode: '',
  },
};
