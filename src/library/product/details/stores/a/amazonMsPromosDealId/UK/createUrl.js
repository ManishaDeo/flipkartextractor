
module.exports = {
  implements: 'product/details/createUrl',
  parameterValues: {
    domain: 'amazon.co.uk',
    url: 'https://www.amazon.co.uk/gp/goldbox?ref_=nav_cs_gb',
    country: 'UK',
    store: 'amazonMsPromosDealId',
    zipcode: '',
  },
};
