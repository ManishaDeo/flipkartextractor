
module.exports = {
  implements: 'product/details',
  parameterValues: {
    country: 'UK',
    store: 'amazonMsPromosDealId',
    domain: 'amazon.co.uk',
    zipcode: '',
  },
};
