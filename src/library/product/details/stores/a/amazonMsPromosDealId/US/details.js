
module.exports = {
  implements: 'product/details',
  parameterValues: {
    country: 'US',
    store: 'amazonMsPromosDealId',
    domain: 'amazon.com',
    zipcode: '',
  },
};
