
module.exports = {
  implements: 'product/details/createUrl',
  parameterValues: {
    domain: 'dyson.co.nz',
    prefix: null,
    url: 'url',
    country: 'NZ',
    store: 'dyson',
    zipcode: '',
  },
};
