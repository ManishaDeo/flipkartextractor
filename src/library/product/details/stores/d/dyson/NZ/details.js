
module.exports = {
  implements: 'product/details',
  parameterValues: {
    country: 'NZ',
    store: 'dyson',
    domain: 'dyson.co.nz',
    zipcode: '',
  },
};
