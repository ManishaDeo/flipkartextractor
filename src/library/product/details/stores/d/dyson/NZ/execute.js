
module.exports = {
  implements: 'product/details/execute',
  parameterValues: {
    country: 'NZ',
    store: 'dyson',
    domain: 'dyson.co.nz',
    loadedSelector: '//div[contains(@class,"layout")]//div[contains(@class,"contents__body")]',
    noResultsXPath: '//div[contains(@class,"row row--tiny")]//h2',
    zipcode: '',
  },
};
