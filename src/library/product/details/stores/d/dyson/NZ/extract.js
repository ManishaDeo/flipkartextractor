const { transform } = require('./shared')
module.exports = {
  implements: 'product/details/extract',
  parameterValues: {
    country: 'NZ',
    store: 'dyson',
    transform,
    domain: 'dyson.co.nz',
    zipcode: '',
  },implementation
};
async function implementation(
  inputs,
  parameters,
  context,
  dependencies,
) {
  const { transform } = parameters;
  const { productDetails } = dependencies;
  await context.evaluate(() => {

    document.getElementsByClassName('price')[1].innerText;
    
    

    })
  return await context.extract(productDetails, { transform });
}
