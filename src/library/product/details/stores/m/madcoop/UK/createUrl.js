
module.exports = {
  implements: 'product/details/createUrl',
  parameterValues: {
    domain: 'butik.mad.coop.dk',
    prefix: null,
    url: null,
    country: 'UK',
    store: 'madcoop',
    zipcode: '',
  },
};
