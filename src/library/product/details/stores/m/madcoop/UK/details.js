
module.exports = {
  implements: 'product/details',
  parameterValues: {
    country: 'UK',
    store: 'madcoop',
    domain: 'butik.mad.coop.dk',
    zipcode: '',
  },
};
