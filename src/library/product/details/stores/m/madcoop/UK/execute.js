
module.exports = {
  implements: 'product/details/execute',
  parameterValues: {
    country: 'UK',
    store: 'madcoop',
    domain: 'butik.mad.coop.dk',
    loadedSelector: null,
    noResultsXPath: null,
    zipcode: '',
  },
};
