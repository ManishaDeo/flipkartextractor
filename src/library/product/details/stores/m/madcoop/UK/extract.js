
module.exports = {
  implements: 'product/details/extract',
  parameterValues: {
    country: 'UK',
    store: 'madcoop',
    transform: null,
    domain: 'butik.mad.coop.dk',
    zipcode: '',
  },
};
