
/**
 *
 * @param { { URL: string, id: any, RPC: string, SKU: string, date: any, days: number, results, zipcode, url} } inputs
 * @param { { store: any, domain: any, country: any, zipcode: any, mergeType: any } } parameters
 * @param { ImportIO.IContext } context
 * @param { { execute: ImportIO.Action, extract: ImportIO.Action, paginate: ImportIO.Action } } dependencies
 */
async function implementation (
  inputs,
  { mergeType, zipcode },
  context,
  { execute, extract, paginate },
) {
  // if days > 0 then days filter is considered
  const { URL: url, RPC, SKU, date: dateOrigin = null, days = 30, results = 150 } = inputs;
  const id = inputs.id = (RPC || SKU || inputs.id);
  inputs.zipcode = inputs.zipcode || zipcode;
  inputs.url = url;
  const length = (results) => results.reduce((acc, { group }) => acc + (Array.isArray(group) ? group.length : 0), 0);
  const date = new Date((days && days > 0) ? new Date().setDate(new Date().getDate() - days) : dateOrigin);
  console.log(`Date Limit: "${date}"`);

  const resultsReturned = await execute(inputs);

  if (!resultsReturned) {
    console.log('No results were returned');
    return;
  }

  const pageOne = await extract(inputs);
  let collected = length(pageOne);

  console.log(`Got initial number of results: ${collected}`);

  // check we have some data
  if (collected === 0) return;
  function checkIfReviewIsWithinMaxDays (data, date) {
    let hasOldReviews = false;
    data.forEach(function (item) {
      item.group.forEach(function (row) {
        const reviewDate = row.reviewDate && row.reviewDate[0] ? new Date(row.reviewDate[0].text).setHours(0, 0, 0, 0) : new Date('01-01-1970');
        if (reviewDate < new Date(date).setHours(0, 0, 0, 0)) {
          hasOldReviews = true;
        }
      });
    });
    return !hasOldReviews;
  }
  if (days && days > 0 && !checkIfReviewIsWithinMaxDays(pageOne, date)) return;

  let page = 2;
  while (results > collected && await paginate({ id, page, offset: collected, date })) {
    const data = await extract(inputs);
    const count = length(data);
    if (count === 0) break; // no results
    collected = (mergeType && (mergeType === 'MERGE_ROWS') && count) || (collected + count);
    console.log('Got more results', collected);
    if (days && days > 0 && !checkIfReviewIsWithinMaxDays(data, date)) break;
    page++;
  }
}
module.exports = {
  parameters: [
    {
      name: 'country',
      description: '2 letter ISO code for the country',
    },
    {
      name: 'store',
      description: 'store name',
    },
    {
      name: 'domain',
      description: 'The top private domain of the website (e.g. amazon.com)',
    },
    {
      name: 'zipcode',
      description: 'to set location',
      optional: true,
    },
    {
      name: 'mergeType',
      description: 'For merge rows results calculation.',
      optional: true,
    },
  ],
  inputs: [
    {
      name: 'URL',
      description: 'direct url for product review page',
      type: 'string',
      optional: true,
    },
    {
      name: 'id',
      description: 'unique identifier for product',
      type: 'string',
      optional: true,
    },
    {
      name: 'RPC',
      description: 'rpc for product',
      type: 'string',
      optional: true,
    },
    {
      name: 'SKU',
      description: 'sku for product',
      type: 'string',
      optional: true,
    },
    {
      name: 'date',
      description: 'earliest date to extract a review',
      type: 'string',
      optional: true,
    },
    {
      name: 'days',
      description: 'reviews from last number of days',
      type: 'number',
      optional: true,
    },
  ],
  dependencies: {
    execute: 'action:product/reviews/execute',
    extract: 'action:product/reviews/extract',
    paginate: 'action:navigation/paginate',
  },
  path: './reviews/stores/${store[0:1]}/${store}/${country}/reviews',
  implementation,
};
