const { transform } = require('../../../../shared');
const { implementation } = require('../GLOBAL/extract');
module.exports = {
  implements: 'product/reviews/extract',
  parameterValues: {
    country: 'CA',
    store: 'amazonMsReviews',
    transform,
    domain: 'amazon.ca',
    zipcode: '',
  },
  implementation,
};
