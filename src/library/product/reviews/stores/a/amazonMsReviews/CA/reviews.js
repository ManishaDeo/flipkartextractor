
module.exports = {
  implements: 'product/reviews',
  parameterValues: {
    country: 'CA',
    store: 'amazonMsReviews',
    domain: 'amazon.ca',
    zipcode: '',
    mergeType: null,
  },
};
