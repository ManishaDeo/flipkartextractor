const { transform } = require('../../../../shared');
const { implementation } = require('../GLOBAL/extract');
module.exports = {
  implements: 'product/reviews/extract',
  parameterValues: {
    country: 'DE',
    store: 'amazonMsReviews',
    transform,
    domain: 'amazon.de',
    zipcode: '',
  },
  implementation,
};
