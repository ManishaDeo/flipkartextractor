
module.exports = {
  implements: 'product/reviews',
  parameterValues: {
    country: 'DE',
    store: 'amazonMsReviews',
    domain: 'amazon.de',
    zipcode: '',
    mergeType: null,
  },
};
