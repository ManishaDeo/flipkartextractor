const { transform } = require('../../../../shared');
const { implementation } = require('../GLOBAL/extract');
module.exports = {
  implements: 'product/reviews/extract',
  parameterValues: {
    country: 'ES',
    store: 'amazonMsReviews',
    transform,
    domain: 'amazon.es',
    zipcode: '',
  },
  implementation,
};
