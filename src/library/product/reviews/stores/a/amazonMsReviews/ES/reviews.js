
module.exports = {
  implements: 'product/reviews',
  parameterValues: {
    country: 'ES',
    store: 'amazonMsReviews',
    domain: 'amazon.es',
    zipcode: '',
    mergeType: null,
  },
};
