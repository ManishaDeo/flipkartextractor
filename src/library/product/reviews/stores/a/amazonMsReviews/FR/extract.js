const { transform } = require('../../../../shared');
const { implementation } = require('../GLOBAL/extract');
module.exports = {
  implements: 'product/reviews/extract',
  parameterValues: {
    country: 'FR',
    store: 'amazonMsReviews',
    transform,
    domain: 'amazon.fr',
    zipcode: '',
  },
  implementation,
};
