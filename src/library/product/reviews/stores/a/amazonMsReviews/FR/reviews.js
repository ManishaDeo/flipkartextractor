
module.exports = {
  implements: 'product/reviews',
  parameterValues: {
    country: 'FR',
    store: 'amazonMsReviews',
    domain: 'amazon.fr',
    zipcode: '',
    mergeType: null,
  },
};
