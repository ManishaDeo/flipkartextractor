const { transform } = require('../../../../shared');
async function implementation (
  inputs,
  parameters,
  context,
  dependencies,
) {
  const { productReviews } = dependencies;
  async function addUrl () {
    function addHiddenDiv (id, content) {
      const newDiv = document.createElement('div');
      newDiv.id = id;
      newDiv.textContent = content;
      newDiv.style.display = 'none';
      document.body.appendChild(newDiv);
    }
    const url = window.location.href;
    addHiddenDiv('added-url', url);
  }
  await context.evaluate(addUrl);
  return await context.extract(productReviews, { type: 'APPEND' });
}
module.exports = {
  implements: 'product/reviews/extract',
  parameterValues: {
    country: 'GLOBAL',
    store: 'amazonMsReviews',
    transform,
    domain: 'amazon.com',
    zipcode: '',
  },
  implementation,
};
