
module.exports = {
  implements: 'product/reviews',
  parameterValues: {
    country: 'GLOBAL',
    store: 'amazonMsReviews',
    domain: 'amazon.com',
    zipcode: '',
    mergeType: null,
  },
};
