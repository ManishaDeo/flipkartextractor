const { transform } = require('../../../../shared');
const { implementation } = require('../GLOBAL/extract');
module.exports = {
  implements: 'product/reviews/extract',
  parameterValues: {
    country: 'IT',
    store: 'amazonMsReviews',
    transform,
    domain: 'amazon.it',
    zipcode: '',
  },
  implementation,
};
