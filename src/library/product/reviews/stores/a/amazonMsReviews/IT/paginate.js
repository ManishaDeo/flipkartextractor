module.exports = {
  implements: 'navigation/paginate',
  parameterValues: {
    country: 'IT',
    store: 'amazonMsReviews',
    loadedSelector: '[data-hook="review"]',
    noResultsXPath: '/html[//*[@id="filter-info-section"]][not(//*[@data-hook="review"])] | /html[//script[contains(text(),\'pageType: "CustomerReviews"\')]][not(//div[@data-hook="review"])] | //div[contains(@class, "page-content") and not(//div[contains(@class, "reviews-content")])] | //div[contains(@class, "no-reviews-section")] | //a[contains(@href, "dogsofamazon")] | //b[contains(@class, "h1") and contains(text(), "particolare")] | //b[contains(@class, "h1") and contains(text(), "Buscas algo")] | //img[contains(@alt, "fetch that page")] | //*[contains(text(),"Looking for something?")] | //a//img[contains(@src,"503.png")] | //a[contains(@href,"ref=cs_503_link")] | //script[contains(text(),"PageNotFound")] | /html[not(//script[contains(text(),\'pageType: "CustomerReviews"\')])] | //*[contains(text(),"Cerchi qualcosa in particolare")] | //b[contains(text(), "Vous recherchez")] | //div[contains(@id,"review_list")  and not( //div[contains(@id,"customer_review")]) ] | //*[contains(text(),"Suchen Sie bestimmte Informationen")] | //img[contains(@alt,  "Sorry")]',
    stopConditionSelectorOrXpath: '//ul[contains(@class,"a-pagination")]/li[contains(@class,"a-disabled") and contains(@class,"a-last")] | //body[not(//ul[contains(@class,"a-pagination")])]',
    openSearchDefinition: {
      template: 'https://www.amazon.it/product-reviews/{id}?sortBy=recent&pageNumber={page}',
    },
    domain: 'amazon.it',
  },
};
