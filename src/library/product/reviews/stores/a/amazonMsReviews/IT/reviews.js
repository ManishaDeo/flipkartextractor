
module.exports = {
  implements: 'product/reviews',
  parameterValues: {
    country: 'IT',
    store: 'amazonMsReviews',
    domain: 'amazon.it',
    zipcode: '',
    mergeType: null,
  },
};
