const { transform } = require('../../../../shared');
const { implementation } = require('../GLOBAL/extract');
module.exports = {
  implements: 'product/reviews/extract',
  parameterValues: {
    country: 'UK',
    store: 'amazonMsReviews',
    transform,
    domain: 'amazon.co.uk',
    zipcode: '',
  },
  implementation,
};
