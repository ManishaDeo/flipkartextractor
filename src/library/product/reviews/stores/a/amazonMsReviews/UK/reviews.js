
module.exports = {
  implements: 'product/reviews',
  parameterValues: {
    country: 'UK',
    store: 'amazonMsReviews',
    domain: 'amazon.co.uk',
    zipcode: '',
    mergeType: null,
  },
};
