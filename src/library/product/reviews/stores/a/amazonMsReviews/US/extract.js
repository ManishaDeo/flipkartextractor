const { transform } = require('../../../../shared');
const { implementation } = require('../GLOBAL/extract');
module.exports = {
  implements: 'product/reviews/extract',
  parameterValues: {
    country: 'US',
    store: 'amazonMsReviews',
    transform,
    domain: 'amazon.com',
    zipcode: '',
  },
  implementation,
};
