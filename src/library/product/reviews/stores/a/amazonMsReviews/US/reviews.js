
module.exports = {
  implements: 'product/reviews',
  parameterValues: {
    country: 'US',
    store: 'amazonMsReviews',
    domain: 'amazon.com',
    zipcode: '',
    mergeType: null,
  },
};
