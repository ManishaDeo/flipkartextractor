
module.exports = {
  implements: 'product/search',
  parameterValues: {
    country: 'CA',
    store: 'amazonMsCategory',
    domain: 'amazon.ca',
    zipcode: '',
    storeID: null,
  },
};
