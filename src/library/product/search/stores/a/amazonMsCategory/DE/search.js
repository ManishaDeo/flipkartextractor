
module.exports = {
  implements: 'product/search',
  parameterValues: {
    country: 'DE',
    store: 'amazonMsCategory',
    domain: 'amazon.de',
    zipcode: '',
    storeID: null,
  },
};
