
module.exports = {
  implements: 'product/search',
  parameterValues: {
    country: 'ES',
    store: 'amazonMsCategory',
    domain: 'amazon.es',
    zipcode: '',
    storeID: null,
  },
};
