
module.exports = {
  implements: 'product/search',
  parameterValues: {
    country: 'FR',
    store: 'amazonMsCategory',
    domain: 'amazon.fr',
    zipcode: '',
    storeID: null,
  },
};
