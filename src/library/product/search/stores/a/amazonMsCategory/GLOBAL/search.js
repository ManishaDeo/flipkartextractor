
module.exports = {
  implements: 'product/search',
  parameterValues: {
    country: 'GLOBAL',
    store: 'amazonMsCategory',
    domain: 'amazon.com',
    zipcode: '',
    storeID: null,
  },
};
