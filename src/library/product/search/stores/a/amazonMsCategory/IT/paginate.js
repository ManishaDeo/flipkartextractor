module.exports = {
  implements: 'navigation/paginate',
  parameterValues: {
    country: 'IT',
    store: 'amazonMsCategory',
    mutationSelector: null,
    spinnerSelector: null,
    loadedSelector: 'ol>li span[class*="item"]>a:nth-child(1)',
    noResultsXPath: '/html[not(//script[contains(text(),\'pageType: "zeitgeist"\')])] | //a//img[contains(@src,"503.png")] | //a[contains(@href,"ref=cs_503_link")] | //script[contains(text(),"PageNotFound")] | //img[contains(@alt,"Dogs of Amazon")] | //*[contains(text(),"Cerchi qualcosa in particolare")] | /html[not(//ol/li)] | //a[contains(@href,"ref=cs_503_link")]',
    stopConditionSelectorOrXpath: '//ul[contains(@class,"a-pagination")]/li[contains(@class,"a-disabled") and contains(@class,"a-last")] | //body[not(//ul[contains(@class,"a-pagination")])]',
    openSearchDefinition: {
      template: 'https://www.amazon.it/gp/bestsellers/*/{id}?_encoding=UTF8&pg={page}',
    },
    domain: 'amazon.it',
  },
};
