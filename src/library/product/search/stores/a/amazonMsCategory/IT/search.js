
module.exports = {
  implements: 'product/search',
  parameterValues: {
    country: 'IT',
    store: 'amazonMsCategory',
    domain: 'amazon.it',
    zipcode: '',
    storeID: null,
  },
};
