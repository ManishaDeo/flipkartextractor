
module.exports = {
  implements: 'product/search',
  parameterValues: {
    country: 'UK',
    store: 'amazonMsCategory',
    domain: 'amazon.co.uk',
    zipcode: '',
    storeID: null,
  },
};
