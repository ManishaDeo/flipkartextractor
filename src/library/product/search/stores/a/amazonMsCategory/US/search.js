
module.exports = {
  implements: 'product/search',
  parameterValues: {
    country: 'US',
    store: 'amazonMsCategory',
    domain: 'amazon.com',
    zipcode: '',
    storeID: null,
  },
};
