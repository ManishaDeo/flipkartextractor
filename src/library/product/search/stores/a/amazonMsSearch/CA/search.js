
module.exports = {
  implements: 'product/search',
  parameterValues: {
    country: 'CA',
    store: 'amazonMsSearch',
    domain: 'amazon.ca',
    zipcode: '',
    storeID: null,
  },
};
