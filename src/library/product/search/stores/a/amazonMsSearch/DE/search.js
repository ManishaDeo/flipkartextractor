
module.exports = {
  implements: 'product/search',
  parameterValues: {
    country: 'DE',
    store: 'amazonMsSearch',
    domain: 'amazon.com',
    zipcode: '',
    storeID: null,
  },
};
