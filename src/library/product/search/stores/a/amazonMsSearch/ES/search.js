
module.exports = {
  implements: 'product/search',
  parameterValues: {
    country: 'ES',
    store: 'amazonMsSearch',
    domain: 'amazon.es',
    zipcode: '',
    storeID: null,
  },
};
