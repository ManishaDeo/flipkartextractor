
module.exports = {
  implements: 'product/search',
  parameterValues: {
    country: 'FR',
    store: 'amazonMsSearch',
    domain: 'amazon.fr',
    zipcode: '',
    storeID: null,
  },
};
