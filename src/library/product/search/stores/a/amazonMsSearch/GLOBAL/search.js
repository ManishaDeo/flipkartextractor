
module.exports = {
  implements: 'product/search',
  parameterValues: {
    country: 'GLOBAL',
    store: 'amazonMsSearch',
    domain: 'amazon.com',
    zipcode: '',
    storeID: null,
  },
};
