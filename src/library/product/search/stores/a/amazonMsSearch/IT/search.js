
module.exports = {
  implements: 'product/search',
  parameterValues: {
    country: 'IT',
    store: 'amazonMsSearch',
    domain: 'amazon.it',
    zipcode: '',
    storeID: null,
  },
};
