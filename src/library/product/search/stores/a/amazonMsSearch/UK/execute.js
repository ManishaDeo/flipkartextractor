
module.exports = {
  implements: 'product/search/execute',
  parameterValues: {
    country: 'UK',
    store: 'amazonMsSearch',
    domain: 'amazon.co.uk',
    url: 'https://www.amazon.co.uk/s?k={searchTerms}&ref=nb_sb_noss_2',
    loadedSelector: 'div[data-asin][data-component-type=s-search-result]',
    noResultsXPath: '/html[not(//script[contains(text(),\'pageType: "Search"\')])] | //a//img[contains(@src,"503.png")] | //a[contains(@href,"ref=cs_503_link")] | //script[contains(text(),"PageNotFound")] | //span[contains(@cel_widget_id,"MAIN-TOP_BANNER_MESSAGE") and contains(., "Keine Ergebnisse")] | /html[not(//div[contains(@data-component-type,"s-search-result") and @data-asin][not(contains(@class, "AdHolder"))])] | //img[contains(@alt,"Dogs of Amazon")] | //*[contains(text(),"Suchen Sie bestimmte Informationen")] | //span[contains(@cel_widget_id,"MAIN-TOP_BANNER_MESSAGE") and contains(., "No hay resultados")] | //*[contains(text(),"Buscas algo")] | /html[not(//div[contains(@data-component-type,"s-search-result")])] | //span[contains(@cel_widget_id,"MAIN-TOP_BANNER_MESSAGE") and contains(., "No results")] | //*[contains(text(),"Looking for something?")] | //span[contains(@cel_widget_id,"MAIN-TOP_BANNER_MESSAGE") and contains(., "Aucun résultat")] | //*[contains(text(),"Vous recherchez une page")] | //span[@dir="auto"][contains(text(),"Nessun risultato per")] | //span[@cel_widget_id="MAIN-TOP_BANNER_MESSAGE" and contains(., "Nessun risultato")] | //*[contains(text(),"Cerchi qualcosa in particolare")] | //span[contains(text(),"sonuç yok")] | //center//a[contains(@href,"ref=cs_503_link")] | //span[@cel_widget_id="MAIN-TOP_BANNER_MESSAGE" and contains(., "No results")]',
    zipcode: '',
  },
};
