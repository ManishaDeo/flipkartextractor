
module.exports = {
  implements: 'product/search',
  parameterValues: {
    country: 'UK',
    store: 'amazonMsSearch',
    domain: 'amazon.co.uk',
    zipcode: '',
    storeID: null,
  },
};
