
module.exports = {
  implements: 'product/search',
  parameterValues: {
    country: 'US',
    store: 'amazonMsSearch',
    domain: 'amazon.com',
    zipcode: '',
    storeID: null,
  },
};
