
module.exports = {
  implements: 'product/search/execute',
  parameterValues: {
    country: 'NZ',
    store: 'dyson',
    domain: 'dyson.co.nz',
    url: 'https://www.dyson.co.nz/catalogsearch/result/?q={searchTerms}',
    loadedSelector: null,
    noResultsXPath: '//div[contains(@class,"row row--tiny")]//h2',
    zipcode: '',
  },
};
