
module.exports = {
  implements: 'product/search/extract',
  parameterValues: {
    country: 'NZ',
    store: 'dyson',
    transform: null,
    domain: 'dyson.co.nz',
    zipcode: '',
  },
};
