
module.exports = {
  implements: 'navigation/paginate',
  parameterValues: {
    template: null,
    country: 'NZ',
    store: 'dyson',
    nextLinkSelector: null,
    nextPageUrlSelector: null,
    nextLinkXpath: null,
    mutationSelector: null,
    spinnerSelector: null,
    loadedSelector: null,
    loadedXpath: '//div[contains(@class,"layout")]//li[contains(@class,"search-results__result")]',
    noResultsXPath: '//div[contains(@class,"row row--tiny")]//h2',
    stopConditionSelectorOrXpath: null,
    resultsDivSelector: null,
    openSearchDefinition: null,
    domain: 'dyson.co.nz',
    zipcode: '',
  },
};
