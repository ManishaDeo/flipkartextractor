module.exports = {
  implements: 'product/search',
  parameterValues: {
    country: 'NZ',
    store: 'dyson',
    domain: 'dyson.co.nz',
    zipcode: '',
    storeID: null,
    mergeType: null,
  },
};
