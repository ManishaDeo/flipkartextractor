
module.exports = {
  implements: 'product/search/execute',
  parameterValues: {
    country: 'IN',
    store: 'flipkart',
    domain: 'flipkart.com',
    url: 'https://www.flipkart.com/search?q={searchTerms}',
    loadedSelector: 'div.CXW8mj',
    noResultsXPath: '//div[contains(@class,"_1sHuca")]/div',
    zipcode: '',
  },
};
