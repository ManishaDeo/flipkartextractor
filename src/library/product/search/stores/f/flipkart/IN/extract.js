
module.exports = {
  implements: 'product/search/extract',
  parameterValues: {
    country: 'IN',
    store: 'flipkart',
    transform: null,
    domain: 'flipkart.com',
    zipcode: '',
  },
  implementation,
};
async function implementation (
  inputs,
  parameters,
  context,
  dependencies,
) {
  const { transform } = parameters;
  const { productDetails } = dependencies;
    await context.evaluate(()=>{
    [...document.querySelectorAll('div._30jeq3._1_WHN1')].forEach(node=>{
      node.innerText = node.innerText.replace('₹','$')
    })
    });
  return await context.extract(productDetails, { transform });
}
