
module.exports = {
  implements: 'navigation/paginate',
  parameterValues: {
    template: null,
    country: 'IN',
    store: 'flipkart',
    nextLinkSelector: null,
    nextPageUrlSelector: null,
    nextLinkXpath: null,
    mutationSelector: null,
    spinnerSelector: null,
    loadedSelector: 'div.CXW8mj',
    loadedXpath: null,
    noResultsXPath: '//div[contains(@class,"_1sHuca")]/div',
    stopConditionSelectorOrXpath: null,
    resultsDivSelector: null,
    openSearchDefinition: null,
    domain: 'flipkart.com',
    zipcode: '',
  },
};
