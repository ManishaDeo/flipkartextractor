
module.exports = {
  implements: 'product/search',
  parameterValues: {
    country: 'IN',
    store: 'flipkart',
    domain: 'flipkart.com',
    zipcode: '',
    storeID: null,
    mergeType: null,
  },
};
